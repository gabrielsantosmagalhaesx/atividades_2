namespace Exercicio_5 
{
    const data = new Date();
    const hora = data.getHours();

    const nome = "Leonardo";

    if(hora >= 6 && hora < 12)
    {
        console.log("Bom dia! " + nome);
    } else if(hora >= 12 && hora < 18)
    {
        console.log("Boa tarde! " + nome);
    } else {
        console.log("Boa noite! " + nome);
    }
}

